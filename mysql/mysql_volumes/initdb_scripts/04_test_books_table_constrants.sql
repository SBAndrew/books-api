USE books_api_db;

CREATE TABLE IF NOT EXISTS debug(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  test_name VARCHAR(255) NOT NULL,
  expected_error_code VARCHAR(255),
  error_code VARCHAR(255),
  error_message VARCHAR(255),
  passed BOOLEAN NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

DELIMITER //

CREATE PROCEDURE IF NOT EXISTS test_books_on_insert(
  IN title VARCHAR(255),
  IN author VARCHAR(255),
  IN publication_year INT UNSIGNED,
  IN genre VARCHAR(128),
  IN isbn VARCHAR(20),
  IN available BOOLEAN,
  IN expected_error_code VARCHAR(64)
)
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION -- if error occures, handler will be executed
  BEGIN
    GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, -- Retrieve the error code
                            @error_message = MESSAGE_TEXT; -- Retrieve the error message
  END;

  BEGIN
    SET @test_name = 'test_books_on_insert';
    SET @sqlstate = '00000';
    SET @error_message = 'no error';

    INSERT INTO books (title, author, publication_year, genre, isbn, available)
    VALUES (title, author, publication_year, genre, isbn, available);

    SET @passed = (@sqlstate = expected_error_code);

    SELECT @test_name as test_name, expected_error_code as expected_error_code, @sqlstate as error_code, @error_message as error_message, @passed as passed; -- print result

    INSERT INTO debug (test_name, expected_error_code, error_code, error_message, passed) 
    VALUES (@test_name, expected_error_code, @sqlstate, @error_message, @passed);

    DELETE FROM books b WHERE b.isbn = isbn;
  END;
END//

DELIMITER ;

CALL test_books_on_insert('Head First Design Patterns', 'Eric Freeman', 2020, 'Computer Science', 'test-0000001', TRUE, '00000'); -- I expect no error
CALL test_books_on_insert('Head First Design Patterns', 'Eric Freeman', 3020, 'Computer Science', 'test-0000002', TRUE, '45100'); -- I expect error
