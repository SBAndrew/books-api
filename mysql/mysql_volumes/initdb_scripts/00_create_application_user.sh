echo "** Creating application user"

# by default, these passwords are already hashed by mysql server
mysql -u root -p"$MYSQL_ROOT_PASSWORD" --execute \
"CREATE USER '$MYSQL_APP_USERNAME'@'localhost' IDENTIFIED BY '$MYSQL_APP_PASSWORD';
CREATE USER '$MYSQL_APP_USERNAME'@'%' IDENTIFIED BY '$MYSQL_APP_PASSWORD';
GRANT ALL ON *.* TO '$MYSQL_APP_USERNAME'@'localhost';
GRANT ALL ON *.* TO '$MYSQL_APP_USERNAME'@'%';
FLUSH PRIVILEGES;"

echo "** Finished creating application user"
