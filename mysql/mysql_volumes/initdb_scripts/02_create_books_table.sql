USE books_api_db;

CREATE TABLE IF NOT EXISTS books (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL, -- sanity checks for title/author, like: can't be the same character only, like: "aaaaaaa", min length, etc
  author VARCHAR(255) NOT NULL,
  publication_year INT UNSIGNED NOT NULL,
  genre VARCHAR(128), -- TO DO: should be an ENUM
  isbn VARCHAR(20) NOT NULL UNIQUE, -- International Standard Book Number, unique for each book (limited to 20 characters)
  available BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


DELIMITER //

CREATE TRIGGER IF NOT EXISTS validate_publication_year_on_insert BEFORE INSERT ON books 
FOR EACH ROW
BEGIN
  SET @current_year = YEAR(CURRENT_TIMESTAMP);
  IF NEW.publication_year > @current_year THEN
    SET @message_text = CONCAT("The publication year can't be higher than the current year, which is ", @current_year, ".");
    SIGNAL SQLSTATE '45100' SET MESSAGE_TEXT = @message_text;
  END IF;
END//

CREATE TRIGGER IF NOT EXISTS validate_publication_year_on_update BEFORE UPDATE ON books 
FOR EACH ROW
BEGIN
  SET @current_year = YEAR(CURRENT_TIMESTAMP);
  IF NEW.publication_year > @current_year THEN
    SET @message_text = CONCAT("The publication year can't be higher than the current year, which is ", @current_year, ".");
    SIGNAL SQLSTATE '45101' SET MESSAGE_TEXT = @message_text;
  END IF;
END//

CREATE PROCEDURE IF NOT EXISTS update_book_if_admin(
  IN username VARCHAR(64),
  IN isbn VARCHAR(20),
  IN title VARCHAR(255),
  IN author VARCHAR(255),
  IN publication_year INT,
  IN genre VARCHAR(255)
)
BEGIN
  DECLARE user_access_level VARCHAR(255);
  SELECT u.access_level INTO user_access_level FROM users u WHERE u.username = username;

  IF user_access_level = 'admin' THEN
    SET @updateStmt = 'UPDATE books SET ';
    
    IF title IS NOT NULL THEN
      SET @updateStmt = CONCAT(@updateStmt, 'title = "', title, '", ');
    END IF;
    IF author IS NOT NULL THEN
      SET @updateStmt = CONCAT(@updateStmt, 'author = "', author, '", ');
    END IF;
    IF publication_year IS NOT NULL THEN
      SET @updateStmt = CONCAT(@updateStmt, 'publication_year = ', publication_year, ', ');
    END IF;
    IF genre IS NOT NULL THEN
      SET @updateStmt = CONCAT(@updateStmt, 'genre = "', genre, '", ');
    END IF;
    
    -- Remove the trailing comma and space
    SET @updateStmt = SUBSTRING(@updateStmt, 1, LENGTH(@updateStmt) - 2);
    
    SET @updateStmt = CONCAT(@updateStmt, ' WHERE isbn = "', isbn, '"');
    
    PREPARE stmt FROM @updateStmt;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
  ELSE
    SIGNAL SQLSTATE '45102' SET MESSAGE_TEXT = 'Only administrators can update books.';
  END IF;
END//

CREATE PROCEDURE IF NOT EXISTS delete_book_cascade(
  IN username VARCHAR(255), 
  IN book_id INT
)
BEGIN
  DECLARE user_access_level VARCHAR(255);
  SELECT u.access_level INTO user_access_level FROM users u WHERE u.username = username;

  IF user_access_level != 'admin' THEN
    SIGNAL SQLSTATE '45103' SET MESSAGE_TEXT = 'Only administrators can delete books.';
  ELSE
    DELETE FROM loans l WHERE l.book_id = book_id;
    DELETE FROM books b WHERE b.id = book_id;
  END IF;
END//

CREATE PROCEDURE IF NOT EXISTS create_book(IN username VARCHAR(255), IN title VARCHAR(255), IN author VARCHAR(255), IN publication_year INT, IN genre VARCHAR(128), IN isbn VARCHAR(20))
BEGIN
  SET @access_level = (SELECT u.access_level FROM users u WHERE u.username = username);
  IF @access_level != 'admin' THEN
    SIGNAL SQLSTATE '45104' SET MESSAGE_TEXT = 'User must be admin to create a book.';
  ELSE
    INSERT INTO books (title, author, publication_year, genre, isbn)
    VALUES (title, author, publication_year, genre, isbn);
  END IF;
END//

DELIMITER ;
