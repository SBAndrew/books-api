CREATE DATABASE IF NOT EXISTS books_api_db;
USE books_api_db;

CREATE TABLE IF NOT EXISTS users (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(64) NOT NULL UNIQUE,
  salt VARCHAR(255) NOT NULL,
  passwd VARCHAR(255) NOT NULL, 
  email VARCHAR(64) NOT NULL UNIQUE,
  access_level ENUM('admin', 'unconfirmed_user') NOT NULL DEFAULT 'unconfirmed_user',
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Disclaimer!
-- in mysql 8.0, you can't create trigger with both INSERT/UPDATE, and can't use NEW.field outside a trigger

DELIMITER //


-- lowercase checks for username, email, access_level

CREATE TRIGGER IF NOT EXISTS lcase_on_insert BEFORE INSERT ON users
FOR EACH ROW
BEGIN
  SET NEW.username = LOWER(NEW.username);
  SET NEW.email = LOWER(NEW.email);
  SET NEW.access_level = LOWER(NEW.access_level);
END//

CREATE TRIGGER IF NOT EXISTS lcase_on_update BEFORE UPDATE ON users
FOR EACH ROW
BEGIN
  SET NEW.username = LOWER(NEW.username);
  SET NEW.email = LOWER(NEW.email);
  SET NEW.access_level = LOWER(NEW.access_level);
END//


-- validate username

CREATE TRIGGER IF NOT EXISTS validate_username_on_insert BEFORE INSERT ON users
FOR EACH ROW
BEGIN
  SET @min_username_length = 5;
  IF CHAR_LENGTH(NEW.username) < @min_username_length THEN
    SET @message_text = CONCAT('Username must have a minimum of ', @min_username_length, ' characters.');
    SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = @message_text;
  END IF;
END//

CREATE TRIGGER IF NOT EXISTS validate_username_on_update BEFORE UPDATE ON users
FOR EACH ROW
BEGIN
  SET @min_username_length = 5;
  IF CHAR_LENGTH(NEW.username) < @min_username_length THEN
    SET @message_text = CONCAT('Username must have a minimum of ', @min_username_length, ' characters.');
    SIGNAL SQLSTATE '45002' SET MESSAGE_TEXT = @message_text;
  END IF;
END//


-- validate email

CREATE TRIGGER IF NOT EXISTS validate_email_on_insert BEFORE INSERT ON users
FOR EACH ROW
BEGIN
  SET @min_email_length = 5;
  IF CHAR_LENGTH(NEW.email) < @min_email_length THEN
    SET @message_text = CONCAT('Email must have a minimum of ', @min_email_length, ' characters.');
    SIGNAL SQLSTATE '45003' SET MESSAGE_TEXT = @message_text;
  END IF;
  IF NEW.email NOT LIKE '%@%' THEN
    SIGNAL SQLSTATE '45004' SET MESSAGE_TEXT = 'Invalid email format. Email must contain @ symbol.';
  END IF;
END//

CREATE TRIGGER IF NOT EXISTS validate_email_on_update BEFORE UPDATE ON users
FOR EACH ROW
BEGIN
  SET @min_email_length = 5;
  IF CHAR_LENGTH(NEW.email) < @min_email_length THEN
    SET @message_text = CONCAT('Email must have a minimum of ', @min_email_length, ' characters.');
    SIGNAL SQLSTATE '45005' SET MESSAGE_TEXT = @message_text;
  END IF;
  IF NEW.email NOT LIKE '%@%' THEN
    SIGNAL SQLSTATE '45006' SET MESSAGE_TEXT = 'Invalid email format. Email must contain @ symbol.';
  END IF;
END//

DELIMITER ;
