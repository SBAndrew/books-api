USE books_api_db;

CREATE TABLE IF NOT EXISTS loans (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  book_id INT NOT NULL,
  loan_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  return_date TIMESTAMP NOT NULL,
  returned BOOLEAN NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (book_id) REFERENCES books (id)
);


DELIMITER //

CREATE TRIGGER IF NOT EXISTS check_book_available_on_insert BEFORE INSERT ON loans
FOR EACH ROW
BEGIN
  SET @book_is_available := (SELECT b.available FROM books b WHERE b.id = NEW.book_id);
  IF @book_is_available != TRUE THEN
    SELECT b.id, b.title, b.author, b.publication_year, b.isbn INTO @book_id, @book_title, @book_author, @book_publication_year, @book_isbn FROM books b WHERE b.id = NEW.book_id;
    SET @message_text = CONCAT("The book with isbn: ", @book_isbn, " is not available.");
    SIGNAL SQLSTATE '45200' SET MESSAGE_TEXT = @message_text;
  ELSE
    UPDATE books b SET b.available = FALSE WHERE b.id = NEW.book_id;
  END IF;
END//

-- CREATE TRIGGER IF NOT EXISTS check_book_available_on_update BEFORE UPDATE ON loans
-- FOR EACH ROW
-- BEGIN
--   SET @book_is_available := (SELECT b.available FROM books b WHERE b.id = NEW.book_id);
--   IF @book_is_available != TRUE THEN
--     SELECT b.id, b.title, b.author, b.publication_year, b.isbn INTO @book_id, @book_title, @book_author, @book_publication_year, @book_isbn FROM books b WHERE b.id = NEW.book_id;
--     SET @message_text = CONCAT("The book with isbn: ", @book_isbn, " is not available.");
--     SIGNAL SQLSTATE '45201' SET MESSAGE_TEXT = @message_text;
--   ELSE
--     UPDATE books b SET b.available = FALSE WHERE b.id = NEW.book_id;
--   END IF;
-- END//

CREATE TRIGGER IF NOT EXISTS check_user_loans_limit_on_insert BEFORE INSERT ON loans
FOR EACH ROW
BEGIN
  SET @loans_limit = 3;
  SET @user_current_loans = (SELECT COUNT(*) FROM loans l WHERE l.user_id = NEW.user_id AND l.returned = FALSE);
  IF @user_current_loans >= @loans_limit THEN
    SET @message_text = CONCAT("User already loaned ", @user_current_loans, " books. The current limit is ", @loans_limit, ".");
    SIGNAL SQLSTATE '45202' SET MESSAGE_TEXT = @message_text;
  END IF;
END//

CREATE TRIGGER IF NOT EXISTS check_user_loans_limit_on_update BEFORE UPDATE ON loans
FOR EACH ROW
BEGIN
  SET @loans_limit = 3;
  SET @user_current_loans = (SELECT COUNT(*) FROM loans l WHERE l.user_id = NEW.user_id AND l.returned = FALSE);
  IF @user_current_loans >= @loans_limit THEN
    SET @message_text = CONCAT("User already loaned ", @user_current_loans, " books. The current limit is ", @loans_limit, ".");
    SIGNAL SQLSTATE '45203' SET MESSAGE_TEXT = @message_text;
  END IF;
END//

CREATE PROCEDURE IF NOT EXISTS create_loan(IN username VARCHAR(255), IN isbn VARCHAR(20), IN return_date TIMESTAMP)
BEGIN
  SET @user_id = (SELECT u.id FROM users u WHERE u.username = username);
  SET @book_id = (SELECT b.id FROM books b WHERE b.isbn = isbn);
  INSERT INTO loans (user_id, book_id, return_date) 
  VALUES (@user_id, @book_id, return_date);
END//

CREATE PROCEDURE IF NOT EXISTS read_all_loans(IN username VARCHAR(255))
BEGIN
  SET @access_level = (SELECT u.access_level FROM users u WHERE u.username = username);
  IF @access_level != 'admin' THEN
    SIGNAL SQLSTATE '45204' SET MESSAGE_TEXT = 'User must be admin to read all loans.';
  ELSE
    SELECT l.id, l.user_id, l.book_id, l.loan_date, l.return_date, l.returned, l.created_at, l.updated_at FROM loans l;
  END IF;
END//

CREATE PROCEDURE IF NOT EXISTS read_user_loans(IN username VARCHAR(64), IN returned_books BOOLEAN) 
BEGIN 
  SELECT b.title, b.author, b.publication_year, b.genre, b.isbn 
  FROM books b 
  INNER JOIN loans l ON l.book_id = b.id 
  INNER JOIN users u ON l.user_id = u.id 
  WHERE u.username = username AND l.returned = returned_books;
END//


DELIMITER ;
