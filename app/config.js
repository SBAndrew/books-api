const config = {
  mysql: {
    host: process.env.MYSQL_HOST || "localhost",
    port: process.env.MYSQL_PORT || "3306",
    database: process.env.MYSQL_DB || "",
    username: process.env.MYSQL_APP_USERNAME || "",
    password: process.env.MYSQL_APP_PASSWORD || "",
  },
  app: {
    port: process.env.APP_PORT || "3000",
    corsWhitelist: [`http://localhost:${process.env.PORT || "3000"}`, "http://localhost:3001", "http://example2.com", ],
    jwtSecret: process.env.JWT_SECRET || "CHANGE-ME-ASAP",
  }
};

module.exports = config;
