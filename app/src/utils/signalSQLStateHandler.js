const signalSQLStateHandler = (err) => {
  if (!err.original) return false

  switch (err.original.sqlState) {
    case '23000':
      return {
        statusCode: 400,
        message: `Duplicate or unavailable data.`, // for more info, extract from err.original.message 
      }
    case '45001':
      return {
        statusCode: 400,
        message: err.original.message,
      }
    case '45002':
      return {
        statusCode: 400,
        message: err.original.message,
      }
    case '45003':
      return {
        statusCode: 400,
        message: err.original.message,
      }
    case '45004':
      return {
        statusCode: 400,
        message: err.original.message,
      }
    case '45005':
      return {
        statusCode: 400,
        message: err.original.message,
      }
    case '45006':
      return {
        statusCode: 400,
        message: err.original.message,
      }

    case '45100':
      return {
        statusCode: 400,
        message: err.original.message,
      }
    case '45101':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45102':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45103':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45104':
      return {
        statusCode: 400,
        message: err.original.message,
    }

    case '45200':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45201':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45202':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45203':
      return {
        statusCode: 400,
        message: err.original.message,
    }
    case '45204':
      return {
        statusCode: 400,
        message: err.original.message,
    }

    default:
      return false
  }
}

module.exports = signalSQLStateHandler;
