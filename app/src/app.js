const path = require("path"); // paths on different os
require("dotenv").config({ path: path.resolve(__dirname, "./../../.env") }); // this will no longer function while dockerized. Use docker compose environment_variables to read from .env
const config = require(path.resolve(__dirname, "./../config"));

const morgan = require("morgan"); // automatic logs on req/res
const logger = require(path.resolve(__dirname, "./utils/logger"));

const helmet = require("helmet"); // improves Express security by setting some HTTP headers
const cors = require("cors"); // allows restricted resources on a web page to be requested from another domain

const express = require("express");
const usersRouter = require(path.resolve(__dirname, "./routes/users"));
const booksRouter = require(path.resolve(__dirname, "./routes/books"));
const loansRouter = require(path.resolve(__dirname, "./routes/loans"));

const corsOptions = {
  origin: (origin, callback) => {
    if (config.app.corsWhitelist.indexOf(origin) !== -1) {
      // checks if the host stored in the 'Origin' header exists in the whitelist
      callback(null, { origin: true });
    } else {
      callback(`Not allowed`, { origin: false });
    }
  },
  optionsSuccessStatus: 200,
};
const app = express();

app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(helmet());
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/v1/users", usersRouter);
app.use("/api/v1/books", booksRouter);
app.use("/api/v1/loans", loansRouter);

app.get("/ping", (req, res) => {
  res.status(200).send("pong");
});

app.listen(config.app.port, () => {
  logger.info(
    `NodeJS Web Server with Express Web Framework started on port: ${config.app.port}`
  );
});

module.exports = app;
