const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./../../../.env") });
const config = require(path.resolve(__dirname, "./../../config"));
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const jsonwebtoken = require("jsonwebtoken");
const crypto = require("crypto");

const signalSQLStateHandler = require(path.resolve(__dirname, "./../utils/signalSQLStateHandler"));
const User = require(path.resolve(__dirname, "./../models/users"));
const usersController = {};

usersController.create = async (req, res, next) => {
  try {
    let { username, passwd, email } = req.body;
    const salt = crypto.randomBytes(16).toString("hex");
    const saltedPassword = salt + passwd;
    const hashedPassword = crypto.createHash("sha256").update(saltedPassword).digest("hex");

    await User.create({ username: username, salt: salt, passwd: hashedPassword, email: email })
    return res.sendStatus(201);
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

usersController.login = async (req, res, next) => {
  try {
    const { username, passwd } = req.body;
    const result = await User.findOne({ where: { username: username } });
    if (!result) return res.sendStatus(404);

    const saltedPassword = result.salt + passwd;
    const hashedPassword = crypto.createHash("sha256").update(saltedPassword).digest("hex");
    if (result.passwd !== hashedPassword) return res.sendStatus(401);

    const jwt = jsonwebtoken.sign({ username: username }, config.app.jwtSecret);
    return res.status(200).json({ jwt: jwt });
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

usersController.errorHandler = (err, req, res, next) => {
  logger.error(err.stack);
  return res.sendStatus(500);
}

module.exports = usersController;
