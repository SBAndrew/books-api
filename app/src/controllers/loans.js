const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./../../../.env") });
const config = require(path.resolve(__dirname, "./../../config"));
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const jsonwebtoken = require("jsonwebtoken");
const signalSQLStateHandler = require(path.resolve(__dirname, "./../utils/signalSQLStateHandler"));
const booksApiDb = require(path.resolve(__dirname, "./../connectors/mysql")).getConnector(config.mysql);
const Loans = require(path.resolve(__dirname, "./../models/loans"));
const loansController = {};

loansController.validateJwt = (req, res, next) => {
  if (!req.headers.authorization) return res.sendStatus(401);
  const jwt = req.headers.authorization.split(" ")[1];
  try {
    const { username } = jsonwebtoken.verify(jwt, config.app.jwtSecret);
    res.locals.username = username;
    next();
  } catch (err) {
    logger.error(err.stack);
    return res.sendStatus(401);
  }
}

loansController.create = async (req, res, next) => {
  try {
    const { isbn, return_date } = req.body;
    await booksApiDb.query(
      'CALL create_loan(:username, :isbn, :return_date)',
      {
        replacements: {
          username: res.locals.username,
          isbn,
          return_date,
        }
      }
    );
    return res.sendStatus(201);
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

loansController.readAll = async (req, res, next) => {
  try {
    const result = await booksApiDb.query(
      'CALL read_all_loans(:username)', 
      {
        replacements: {
          username: res.locals.username,
        }
      }
    );
    return res.status(200).json({ loans: result });
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

loansController.read = async (req, res, next) => {
  try {
    const result = await booksApiDb.query(
      'CALL read_user_loans(:username, :returned_books)',
      {
        replacements: {
          username: res.locals.username,
          returned_books: req.body.returned_books || false,
        }
      }
    );
    return res.status(200).json({ loans: result });
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

loansController.errorHandler = (err, req, res, next) => {
  logger.error(err.stack);
  return res.sendStatus(500);
}

module.exports = loansController;
