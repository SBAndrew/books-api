const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./../../../.env") });
const config = require(path.resolve(__dirname, "./../../config"));
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const jsonwebtoken = require("jsonwebtoken");
const signalSQLStateHandler = require(path.resolve(__dirname, "./../utils/signalSQLStateHandler"));
const booksApiDb = require(path.resolve(__dirname, "./../connectors/mysql")).getConnector(config.mysql);
const Books = require(path.resolve(__dirname, "./../models/books"));
const booksController = {};

booksController.validateJwt = (req, res, next) => {
  try {
    if (!req.headers.authorization) return res.sendStatus(401);
    const jwt = req.headers.authorization.split(" ")[1];
    const { username } = jsonwebtoken.verify(jwt, config.app.jwtSecret);
    res.locals.username = username;
    next();
  } catch (err) {
    logger.error(err.stack);
    return res.sendStatus(401);
  }
}

booksController.create = async (req, res, next) => {
  try {
    const { title, author, publication_year, genre, isbn } = req.body;
    await booksApiDb.query(
      'CALL create_book(:username, :title, :author, :publication_year, :genre, :isbn)',
      {
        replacements: {
          username: res.locals.username,
          title,
          author,
          publication_year,
          genre,
          isbn,
        }
      },
    );

    return res.sendStatus(201);
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

booksController.readAll = async (req, res, next) => {
  try {
    const result = await Books.findAll();
    return res.status(200).json({ books: result });
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

booksController.update = async (req, res, next) => {
  try {
    await booksApiDb.query(
      'CALL update_book_if_admin(:username, :isbn, :title, :author, :publication_year, :genre)',
      {
        replacements: {
          username: res.locals.username,
          isbn: req.body.isbn,
          title: req.body.title || null, // only null validates the IF from the stored procedure
          author: req.body.author || null,
          publication_year: req.body.publication_year || null,
          genre: req.body.genre || null,
        }
      }
    )
    return res.sendStatus(200);
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

booksController.delete = async (req, res, next) => {
  try {
    await booksApiDb.query(
      'CALL delete_book_cascade(:username, :book_id)',
      {
        replacements: {
          username: res.locals.username,
          book_id: req.body.book_id
        }
      }
    )
    return res.sendStatus(200);
  } catch (err) {
    const state = signalSQLStateHandler(err);
    if (state) return res.status(state.statusCode).json({ message: state.message })
    next(err);
  }
}

booksController.errorHandler = (err, req, res, next) => {
  logger.error(err.stack);
  return res.sendStatus(500);
}

module.exports = booksController;
