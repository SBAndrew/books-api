const path = require("path");

const logger = require(path.resolve(__dirname, "./../utils/logger"));
const { Sequelize } = require("sequelize");

function createConnector(host, port, database, username, password) {
  return new Sequelize(database, username, password, {
    host: host,
    port: port,
    pool: {
      max: 5,
      idle: 30_000,
      acquire: 60_000,
    },
    logging: (msg) => logger.info(msg),
    dialect: "mysql",
    retry: {
      max: Infinity,
      match: [
      Sequelize.ConnectionError,
      'SQLITE_BUSY',
      ],
      backoffBase: 10_000,
      backoffExponent: 1.0,
      report: (msg) => logger.warn(msg),
      name: `[${host}/${database}]`,
    },
  });
}

// flyweight design pattern
class ConnectorFactory {
  constructor() {
    this._connectors = {};
  }

  getConnector({ host, port, database, username, password }) {
    const key = [ host, port, database, username ].join(", ");
    if (!this._connectors[key]) {
      this._connectors[key] = createConnector(host, port, database, username, password);
    }
    return this._connectors[key];
  }
}

const connectorFactory = new ConnectorFactory();

module.exports = connectorFactory;
