const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./../../../.env") });
const config = require(path.resolve(__dirname, "./../../config"));
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const { Model, DataTypes } = require("sequelize");
const connectorFactory = require(path.resolve(__dirname, "./../connectors/mysql"));

function createUserModel(sequelize) {
  class User extends Model {}
  User.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      }, 
      username: {
        type: DataTypes.STRING(64),
        allowNull: false,
      },
      salt: {
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: "salt"
      },
      passwd: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      access_level: {
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: "unconfirmed_user",
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "User",
      tableName: "users",
      timestamps: false,
    }
  );

  return User;
}

module.exports = createUserModel(
  connectorFactory.getConnector(config.mysql)
);
