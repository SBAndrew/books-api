const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./../../../.env") });
const config = require(path.resolve(__dirname, "./../../config"));
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const { Model, DataTypes } = require("sequelize");
const connectorFactory = require(path.resolve(__dirname, "./../connectors/mysql"));
const User = require(path.resolve(__dirname, "./users"));
const Book = require(path.resolve(__dirname, "./books"));

function createLoanModel(sequelize) {
  class Loan extends Model {}
  Loan.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      }, 
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: User,
          key: "id",
        },
      },
      book_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: Book,
          key: "id",
        },
      },
      loan_date: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      return_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      returned: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "Loan",
      tableName: "loans",
      timestamps: false,
    }
  );

  return Loan;
}

module.exports = createLoanModel(
  connectorFactory.getConnector(config.mysql)
);
