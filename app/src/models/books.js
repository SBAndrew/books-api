const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./../../../.env") });
const config = require(path.resolve(__dirname, "./../../config"));
const logger = require(path.resolve(__dirname, "./../utils/logger"));

const { Model, DataTypes } = require("sequelize");
const connectorFactory = require(path.resolve(__dirname, "./../connectors/mysql"));

function createBookModel(sequelize) {
  class Book extends Model {}
  Book.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      }, 
      title: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      author: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      publication_year: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      genre: {
        type: DataTypes.STRING(128),
        allowNull: false,
      },
      isbn: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      available: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      sequelize,
      modelName: "Book",
      tableName: "books",
      timestamps: false,
    }
  );

  return Book;
}

module.exports = createBookModel(
  connectorFactory.getConnector(config.mysql)
);
