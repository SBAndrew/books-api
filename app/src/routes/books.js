const path = require("path");

const booksRouter = require("express").Router();
const booksController = require(path.resolve(__dirname, "./../controllers/books"));

booksRouter
  .use(booksController.validateJwt)

booksRouter
  .route("/create")
  .post(booksController.create)

booksRouter
  .route("/readAll")
  .get(booksController.readAll)

booksRouter
  .route("/update")
  .patch(booksController.update)

booksRouter
  .route("/delete")
  .delete(booksController.delete)

booksRouter
  .use(booksController.errorHandler)

module.exports = booksRouter;
