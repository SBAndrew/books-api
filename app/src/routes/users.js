const path = require("path");

const usersRouter = require("express").Router();
const usersController = require(path.resolve(__dirname, "./../controllers/users"));

usersRouter
  .route("/create")
  .post(usersController.create)

usersRouter
  .route("/login")
  .post(usersController.login)

usersRouter
  .use(usersController.errorHandler)

module.exports = usersRouter;
