const path = require("path");

const loansRouter = require("express").Router();
const loansController = require(path.resolve(__dirname, "./../controllers/loans"));

loansRouter
  .use(loansController.validateJwt)

loansRouter
  .route("/create")
  .post(loansController.create)

loansRouter
  .route("/readAll")
  .get(loansController.readAll)

loansRouter
  .route("/read")
  .get(loansController.read)

loansRouter
  .use(loansController.errorHandler)

module.exports = loansRouter;
