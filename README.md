# Books-Api

## Name
Books API Prototype

## Description
This project is a prototype of what a simple Express.js MVC API could look like, for 2 resources: users and books. Features that I would like to highlight are: 
- triggers for data validation on all tables
- stored procedures for manipulating data based on access_level of an user, for cascade delete and for testing the constraints on a table
- FlyWeight design pattern for managing db connections
- MVC design pattern for structuring the REST API
- jwt authentication for users
- sha256 encription for user passwords

## Installation
- make sure you have Docker installed
- clone the repo
- create a .env file near the books-api/.env.example 
- add environment variables into the .env specified in the books-api/.env.example

## Usage
- open a console
- run the startup.sh file with the following command: sh startup.sh
- wait a minute in order for the containers to fully initialize. MySQL has to run some setup scripts and then to restart
- import the books-api.postman_collection.json into Postman 

## Project status
- might play with it later on
